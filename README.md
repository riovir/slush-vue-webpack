# DEPRECATED: Slush Slush-vue-webpack
> Used to be Vue + Webpack + Bulma scaffolding for Slush

Since NPM already supports project initializers, the code has been migrated to:
* Project initializer: https://gitlab.com/riovir/create-vue-app
* Project template: https://gitlab.com/riovir/vue-app-scaffolding

## Try it out

```bash
mkdir my-app
cd my-app
npm init @riovir/vue-app
npm i
npm run dev
# See localhost:3000 in browser
```
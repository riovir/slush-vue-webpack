v0.1.0:
  date: 2016-11-10
  changes:
    - Initial release.
    
# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [9.0.0] - 2019-03-13
### End of life
The scaffolding has moved to https://www.npmjs.com/package/@riovir/create-vue-app removing the need to install `gulp` or `slush`.

## [8.3.0] - 2018-08-06
### Added
* **Vuex** Is now used by default for state management
* **babel** `Stage 3` features have been enabled to allow using the `rest/spread operator`

## [8.2.3] - 2018-08-06
### Added
* **webpack.base.config.js** `sourcemap` option for style loaders
* **sass** added `resolve-url-loader` to fix relative `url` imports in `sass` / `scss` files

## [8.2.2] - 2018-08-02
### Removed
* **express-static** Unused dependency which has been replaced by the built-in middleware a while ago

## [8.2.1] - 2018-08-02
### Fixed
* **Scaffolding** Reverted string templates as it tipped off the template compiler

## [8.2.0] - 2018-08-02
### Added
* **PostCSS** Turn on `Stage 3+` features including `CSS grids` with `postcss-preset-env`
* **Networking** Some clarification on how to proxy requests to 3rd party APIs

### Changed
* **ESlint** Fixed `/server` directory

### Fixed
* **Bulma** Modal dialog can now be closed with the `X` in the top right corner. It's also centerd in `IE11` now.

## [8.1.1] - 2018-08-02
### Changed
* **package.json** Updated to latest dependencies
* **jest** Set `testURL` to make tests using `localStorage` pass

## [8.1.0] - 2018-07-20
### Changed
* **test** Moved `specs` together with source. This results in less up and down scrolling when doing TDD as the repo grows.
* **ESlint** Converted format to `YAML`. The extensionless `.eslintrc` has been deprecated anyway: https://eslint.org/docs/user-guide/configuring#configuration-file-formats

## [8.0.0] - 2018-07-19
### Breaking changes
* Replaced `Karma` + `Mocha` + `Sinon` + `Chai` + `Webpack test config` with `Jest`

### Added
* **Jest** Now supporting parallel test run, ability to only run failed / specific tests and DOM isolation

### Changed
* **package.json** Updated to latest dependencies

## [7.2.5] - 2018-07-03
### Fixed
* **package.json** Fix public path not being properly set by prompts.

## [7.2.4] - 2018-06-27
* Updated dependencies to latest major version

## [7.2.3] - 2018-06-27
* Updated dependencies to latest stable version

### Changed
* `webpack.base.config.js` no longer adds style related rules in test mode
* `webpack.test.config.js` also ignore `sass` and `scss` extensions

## [7.2.2] - 2018-06-19
### Fixed 
* **gulp-install** is no longer being used. It already didn't work and it started to interfere with the generation.

## [7.2.1] - 2018-06-19
### Changed
* **Font Awesome** CSS is explicitly bundled instead of being runtime injected with inline styles
* **cs-icon** has been renamed to **td-icon**

## [7.2.0] - 2018-06-19
### Changed 
* **.editorconfig** now uses tabs by default by popular request
* **.eslintrc** warns on not indenting with tabs
* **app** extracted a `component` and a `widget` as an example of breaking down large components

### Added
* **Font Awesome 5** icon support with example

## [7.1.1] - 2018-06-12
### Fixed
* `webpack.base.config.js` misleading the `gulp-template` engine

## [7.1.0] - 2018-06-12
### Added
* **Babel** Now uses white listing over banning `node_modules` to allow cherry bundling some packages from source, such as `Buefy`
* **Buefy** components are now cherry picked and bundled in to reduce bundle size
* **Bulma** theming and override support added via `src/styles`
* **Vue** files can now be imported without extension and can represent a directory

## [7.0.0] - 2018-06-12
### Breaking changes
* Updated linter rules to use recommended Vue.js settings: https://vuejs.org/v2/style-guide/

### Changed
* Recommended project structure, for smaller files
* `src/store/index.js` uses separate functions to allow them calling each other without using `this`
* `gateway.js` moved into `store` as an implementation detail of it

## [6.1.0] - 2018-06-12
### Changed
* Replace `PhantomJs` with `Jsdom` for smaller test footprint. Also disabling `Vue` transitions, as `Jsdom` does not support them

## [6.0.10] - 2018-06-12
* Updated dependencies to latest stable version

## [6.0.9] - 2018-04-25
* ```.eslintrc``` readd missing files

## [6.0.8] - 2018-04-25
* Update ```vue-loader```, ```mini-css-extract-plugin```, and ```bulma``` to latest major version
* Add content hash to generated bundles

## [6.0.7] - 2018-04-25
* Updated dependencies to latest stable version
* Added example of store pattern, unit and integration testing

## [6.0.6] - 2018-03-22
### Fixed
* **vue-loader** Fixed ignored less compilation setting


## [6.0.5] - 2018-03-22
### Fixed
* **app.vue** Corrected public path setting location

## [6.0.4] - 2018-03-22
### Fixed
* **.eslintrc** Split by context
* **test** Fixed typo in comments

## [6.0.3] - 2018-03-22
### Changed
* **babel** no longer compiles code to ```CommonJs``` modules
### Fixed
* **webpack.config** relocated ```devServer``` config
* **webpack.test.config** Removed reference of Semantic UI
* **main** Fixed lint error
## [6.0.2] - 2018-03-22
### Fixed
* **vue-loader** No longer uses a template compiler at runtime, instead templates in ```.vue``` components are converted to render functions.

## [6.0.1] - 2018-03-22
### Fixed
* **package.json** Removed leftover Semantic UI fix script

## [6.0.0] - 2018-03-22
### Breaking changes
* **semantic-ui-less** Has been replaced by ```Bulma```
* **jquery** Has been removed

### Added
* **bulma** To replace ```Semantic UI``` with a more modern, light weight alternative
* **buefy** For ```Vue.js``` convenience to use ```Bulma```
* **vue-focus-trap** To address lack of focus trapping behavior in modals

## [5.5.2] - 2018-03-22
### Changed
* Updated generator dependencies to latest version


## [5.5.1] - 2018-03-22
### Changed
* Updated dependencies to latest version

## [5.5.0] - 2018-03-10
### Changed
* Updated to ```Webpack 4```
* Replaced ```extract-text-webpack-plugin``` with ```mini-css-extract-plugin```
* Moved ```HotModuleReplacementPlugin``` out of ```base``` config, as tests don't need it
### Fixed
* Warnings in ```.eslintrc```

## [5.4.0] - 2017-11-24
FYI: a ```public path``` is where your site is expected to be served from.
Eg. if using ```GitLab pages``` your site will be served from ```https://yourname.gitlab.io/your-project/```. In this case your public path is ```/your-project/```.
### Added
- **webpack** Public path can now be configured outside of source code
  - in ```package.json > config``` to set the default
  - which can be overridden by the ```PUBLIC_PATH``` environment variable
- **vue-loader** now extracts CSS from components. This reduces problems when using stricter ```Content-Security-Policy``` headers

### Fixed
- **server** ```npm run prod``` works again, also now redirects to the ```public path``` from the ```/```

## [5.3.0] - 2017-10-27
### Removed
- **package.json**
  - ```json-loader```, ```shortid```, and ```style-loader``` as they were not required (Webpack can now load JSON files out of box)
  - ```npm prune``` postinstall script: it used to be a convenience so that devs don't forget to --save[-dev] install ed packages. ```NPM 5``` now does this out of box

### Changed
- **package.json** Updated dependencies to latest versions
- **.babelrc** replaced ```babel-preset-es2015``` with ```babel-preset-env``` as per the recommended here: ```babeljs.io/env```
- **webpack-dev-server** Now allows connections outside from ```localhost```

### Fixed
- **/test/index.js** Now ignores ```index.html``` and no longer needs the ```webpack.test.config.js``` to use the ```null-loader``` for it causing warnings
- **/dev-server.config.js** Warning due to using deprecated ```setup``` hook

## [5.2.1] - 2017-09-05
### Fixed
- **webpack.test.config** Fixed failing test by not loading HTML files

## [5.2.0] - 2017-09-05
### Fixed
- **eslint** Ignore ```node_modules``` and ```dist```
- **dev-server.config**, **wepback.config** Fixed broken public path: The ```dist``` should not be included in it

### Added
- **HtmlWebpackPlugin** to infer and required CSS and JS. **CopyWebpackPlugin** is no longer necessary
- **CommonsChunkPlugin** to automatically split out vendor code (all node_modules and bundled Semantic UI style)

### Changed
- **package.json** Updated dependencies to latest versions
- **npm run dev** No longer auto-opens a browser tab: the ```--open``` is not a stable flag recently

## [5.1.1] - 2017-08-10
### Fixed
- **defaults** Fixed error on missing git user in config

## [5.1.0] - 2017-08-04
### Added
- **webpack** bundle size analyze ran via ```npm run analyze```

## [5.0.1] - 2017-07-27
### Changed
- **webpack** simplified production build via the ```-p``` option and one less entry
- **vue-loader** downgraded to ```12.2.2``` to avoid ```inject-loader``` based bug

### Fixed
- ```Vue``` not being used warning 

## [5.0.0] - 2017-07-15
### Added
- **Hot Module Reload** turned on with ```webpack-dev-server```

### Changed
- **server.js** moved to its own package as **index.js** only being used for production

### Removed
- Dropped ```concurrently``` and ```reloadify``` as dependencies

## [4.5.2] - 2017-07-13
### Fixed
- **webpack.base.config.js** Fixed broken font and image paths

## [4.5.1] - 2017-07-13
### Changed
- **package.json** Simplified scripts

## [4.5.0] - 2017-07-13
### Changed
- **package.json** Updated to latest dependencies
- Various separation of concerns fixes

## [4.4.0] - 2017-02-27
### Fixed
- **package.json** Fixed prod and dev dependencies

## [4.3.1] - 2017-02-15
### Fixed
- **webpack** Added missing postcss-loader config to allow autoprefixing css

const gulp = require('gulp');

gulp.task('default', done => {
	console.warn('END OF LIFE: Use https://www.npmjs.com/package/@riovir/create-vue-app instead by typing:')
	console.warn('\n\tnpm init @riovir/vue-app\n');
	done();
});
